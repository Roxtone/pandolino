﻿using UnityEngine;

public class EatingAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip slapClip;
    [SerializeField]
    private float pitchVariance = 0.1f;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        audioSource.pitch = Random.Range(1.0f - pitchVariance, 1.0f + pitchVariance);
        audioSource.PlayOneShot(slapClip);
    }
}