﻿using UnityEngine;

public class GameFinishAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip whistleClip;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        GameFinishedEvent.AddListener(OnGameFinished);
    }

    void OnDestroy()
    {
        GameFinishedEvent.RemoveListener(OnGameFinished);
    }

    private void OnGameFinished(GameFinishedEvent eventData)
    {
        audioSource.PlayOneShot(whistleClip);
    }
}