﻿using UnityEngine;

public class MovementAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip swooshClip;
    [SerializeField]
    private float pitchVariance = 0.1f;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        PlayerMovedEvent.AddListener(OnPlayerMoved);
    }

    void OnDestroy()
    {
        PlayerMovedEvent.RemoveListener(OnPlayerMoved);
    }

    private void OnPlayerMoved(PlayerMovedEvent eventData)
    {
        audioSource.pitch = Random.Range(1.0f - pitchVariance, 1.0f + pitchVariance);
        audioSource.PlayOneShot(swooshClip);
    }
}