﻿using System.Collections;
using UnityEngine;

public class RandomAudio : MonoBehaviour
{
    [SerializeField]
    private AudioClip[] clips;
    [SerializeField]
    private float interval = 20.0f;
    [SerializeField]
    private float intervalVariance = 5.0f;

    private AudioSource audioSource;

    void Awake()
    {
        audioSource = GetComponent<AudioSource>();
        StartCoroutine(RandomAudioCoroutine());
    }

    private IEnumerator RandomAudioCoroutine()
    {
        while (true)
        {
            float time = Random.Range(interval - intervalVariance, interval + intervalVariance);
            yield return new WaitForSeconds(time);
            audioSource.PlayOneShot(clips[Random.Range(0, clips.Length)]);
        }
    }
}