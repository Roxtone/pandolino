﻿using System;

public abstract class Event<T> : AbstractEvent where T : AbstractEvent
{
    private static Action<T> listeners = delegate { };

    public static void AddListener(Action<T> listener)
    {
        listeners += listener;
    }

    public static void RemoveListener(Action<T> listener)
    {
        listeners -= listener;
    }

    public static void RemoveAllListeners()
    {
        listeners = delegate { };
    }

    public static void Invoke(T e)
    {
        listeners(e);
    }

    public void Invoke()
    {
        Invoke(this as T);
    }
}

public abstract class AbstractEvent { }