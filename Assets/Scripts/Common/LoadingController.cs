﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadingController : Singleton<LoadingController>
{
    private bool isLoading = false;

    public void LoadScene(string sceneName)
    {
        if (!isLoading)
        {
            StartCoroutine(LoadSceneCoroutine(sceneName));
        }
    }

    private IEnumerator LoadSceneCoroutine(string sceneName)
    {
        isLoading = true;
        Fade.Instance.FadeOut();
        yield return new WaitForSeconds(Fade.Instance.FadeDuration);
        SceneManager.LoadSceneAsync(sceneName);
    }
}