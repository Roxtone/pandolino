﻿public class FlyEatenEvent : Event<FlyEatenEvent>
{
    public Fly Fly { get; set; }
}