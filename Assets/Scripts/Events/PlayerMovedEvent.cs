﻿public class PlayerMovedEvent : Event<PlayerMovedEvent>
{
    public Tile Tile { get; set; }
}