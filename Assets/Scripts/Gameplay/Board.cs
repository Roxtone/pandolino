﻿using UnityEngine;

public class Board : Singleton<Board>
{
    public const int Width = 8;
    public const int Height = 2;
    public const float TileSize = 1.0f;
    public const float MarginX = 0.5f;
    public const float MarginY = 1.0f;

    public Vector3 CalculateTileCenter(int x, int y)
    {
        return new Vector3(x, 0.0f, -y);
    }
}