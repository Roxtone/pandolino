﻿using UnityEngine;

public class ColorManager : Singleton<ColorManager>
{
    [SerializeField]
    private Color[] colors;

    public int ColorCount { get { return colors.Length; } }

    public Color GetColor(int index)
    {
        return colors[index];
    }
}