﻿using System.Collections;
using UnityEngine;

public class ColorObject : MonoBehaviour
{
    [SerializeField]
    private float transitionDuration = 0.0f;

    private MeshRenderer[] meshRenderers;

    protected void ChangeColor(int colorIndex)
    {
        ChangeColor(ColorManager.Instance.GetColor(colorIndex));
    }

    protected void ChangeColor(Color color)
    {
        if (meshRenderers == null)
        {
            meshRenderers = GetComponentsInChildren<MeshRenderer>();
        }
        StopAllCoroutines();
        StartCoroutine(ChangeColorCoroutine(color));
    }

    private IEnumerator ChangeColorCoroutine(Color color)
    {
        Color oldColor = meshRenderers[0].material.color;
        float timer = 0.0f;
        while (transitionDuration > 0.0f && timer < transitionDuration)
        {
            Color currentColor = Color.Lerp(oldColor, color, timer / transitionDuration);
            SetRendererColor(currentColor);
            timer += Time.deltaTime;
            yield return null;
        }
        SetRendererColor(color);
    }

    private void SetRendererColor(Color color)
    {
        for (int i = 0; i < meshRenderers.Length; i++)
        {
            meshRenderers[i].material.color = color;
        }
    }
}