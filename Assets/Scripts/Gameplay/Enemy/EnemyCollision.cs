﻿using UnityEngine;

public class EnemyCollision : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (!GameManager.Instance.IsFinished)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        GameFinishedEvent gameFinishedEvent = new GameFinishedEvent();
        gameFinishedEvent.Invoke();
    }
}