﻿using UnityEngine;

public class EnemyDetection : MonoBehaviour
{
    void OnTriggerStay(Collider other)
    {
        if (!GameManager.Instance.IsFinished && !PlayerHiding.Instance.IsHidden)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        GameFinishedEvent gameFinishedEvent = new GameFinishedEvent();
        gameFinishedEvent.Invoke();
    }
}