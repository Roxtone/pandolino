﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    [SerializeField]
    private float movementSpeed = 1.0f;
    [SerializeField]
    private int startPathIndex = 0;
    [SerializeField]
    private int startPathDirection = 1;
    [SerializeField]
    private int controlDirection = 1;
    [SerializeField]
    private float controlChance = 0.5f;
    [SerializeField]
    private float controlDuration = 2.0f;

    private Vector3[] path;
    private int currentPathIndex;
    private int currentPathDirection;
    private bool isControlling;

    void Awake()
    {
        currentPathIndex = startPathIndex;
        currentPathDirection = startPathDirection;
        CreatePath();
        InitializeRotation();
        InitializePosition();
        GameFinishedEvent.AddListener(OnGameFinished);
    }

    void OnDestroy()
    {
        GameFinishedEvent.RemoveListener(OnGameFinished);
    }

    void Update()
    {
        if (!isControlling && !GameManager.Instance.IsFinished)
        {
            Move();
        }
    }

    private void CreatePath()
    {
        List<Vector3> nodes = new List<Vector3>();
        for (int i = 0; i < Board.Width; i++)
        {
            Vector3 node = new Vector3(i * Board.TileSize, transform.position.y, transform.position.z);
            nodes.Add(node);
        }
        path = nodes.ToArray();
    }

    private void InitializeRotation()
    {
        transform.rotation = Quaternion.Euler(0.0f, currentPathDirection * 90.0f, 0.0f);
    }

    private void InitializePosition()
    {
        transform.position = path[currentPathIndex];
    }

    private void Move()
    {
        float distanceToCover = movementSpeed * Time.deltaTime;
        while (distanceToCover > 0.0f)
        {
            Vector3 toDestination = path[currentPathIndex + currentPathDirection] - transform.position;
            float distance = toDestination.magnitude;
            if (distance < distanceToCover)
            {
                transform.position = path[currentPathIndex + currentPathDirection];
                SelectNextNode();
                if (ShouldControl())
                {
                    StartCoroutine(ControlCoroutine());
                    distanceToCover = 0.0f;
                }
                else
                {
                    distanceToCover -= distance;
                }
            }
            else
            {
                transform.position += distanceToCover * toDestination.normalized;
                distanceToCover = 0.0f;
            }
        }
    }

    private void SelectNextNode()
    {
        currentPathIndex += currentPathDirection;
        if (currentPathIndex == 0 || currentPathIndex == path.Length - 1)
        {
            currentPathDirection = -currentPathDirection;
            Turn();
        }
    }

    private bool ShouldControl()
    {
        return currentPathIndex % 2 == 0 && Random.value < controlChance;
    }

    private IEnumerator ControlCoroutine()
    {
        isControlling = true;
        transform.Rotate(Vector3.up, currentPathDirection * controlDirection * 90.0f);
        yield return new WaitForSeconds(controlDuration);
        transform.Rotate(Vector3.up, -currentPathDirection * controlDirection * 90.0f);
        isControlling = false;
    }

    private void Turn()
    {
        transform.Rotate(Vector3.up, 180.0f);
    }

    private void OnGameFinished(GameFinishedEvent eventData)
    {
        StopAllCoroutines();
    }
}