﻿using UnityEngine;

public class Fly : ColorObject
{
    private int colorIndex;

    void Awake()
    {
        ColorIndex = Random.Range(0, ColorManager.Instance.ColorCount);
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    public int ColorIndex
    {
        get
        {
            return colorIndex;
        }
        set
        {
            colorIndex = value;
            ChangeColor(colorIndex);
        }
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        if (eventData.Fly == this)
        {
            Destroy(gameObject);
        }
    }
}