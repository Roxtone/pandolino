﻿using System.Collections;
using UnityEngine;

public class FlySpawner : MonoBehaviour
{
    [SerializeField]
    private Fly flyPrefab;
    [SerializeField]
    private float height = 2.0f;
    [SerializeField]
    private float interval = 5.0f;
    [SerializeField]
    private float intervalVariance = 1.0f;
    [SerializeField]
    private int prespawnCount = 3;

    void Awake()
    {
        Prespawn();
        StartCoroutine(SpawnCoroutine());
    }

    private void Prespawn()
    {
        for (int i = 0; i < prespawnCount; i++)
        {
            Spawn();
        }
    }

    private IEnumerator SpawnCoroutine()
    {
        while (!GameManager.Instance.IsFinished)
        {
            Spawn();
            float currentInterval = interval + Random.Range(-intervalVariance, intervalVariance);
            yield return new WaitForSeconds(currentInterval);
        }
    }

    private void Spawn()
    {
        Vector3 position = CalculateSpawnPosition();
        Instantiate(flyPrefab, position, Quaternion.identity, transform);
    }

    private Vector3 CalculateSpawnPosition()
    {
        float x = Random.Range(-Board.MarginX, Board.Width * Board.TileSize + Board.MarginX);
        float y = Random.Range(-Board.MarginY, Board.Height * Board.TileSize + Board.MarginY);
        return new Vector3(x, height, -y);
    }
}