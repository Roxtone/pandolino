﻿public class GameManager : Singleton<GameManager>
{
    public bool IsFinished { get; private set; }

    void Awake()
    {
        GameFinishedEvent.AddListener(OnGameFinished);
    }

    void OnDestroy()
    {
        GameFinishedEvent.RemoveListener(OnGameFinished);
    }

    private void OnGameFinished(GameFinishedEvent eventData)
    {
        IsFinished = true;
    }
}