﻿public class PlayerColor : ColorObject
{
    private int colorIndex = -1;

    void Awake()
    {
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    public int ColorIndex
    {
        get
        {
            return colorIndex;
        }
        set
        {
            colorIndex = value;
            ChangeColor(colorIndex);
        }
    }

    private void OnFlyEaten(FlyEatenEvent flyEatenEvent)
    {
        ColorIndex = flyEatenEvent.Fly.ColorIndex;
    }
}