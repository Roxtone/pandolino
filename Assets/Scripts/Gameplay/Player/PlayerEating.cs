﻿using System.Collections;
using UnityEngine;

public class PlayerEating : MonoBehaviour
{
    [SerializeField]
    private float eatingCooldown = 0.25f;
    [SerializeField]
    private float eatingDistance = 3.0f;
    [SerializeField]
    private LayerMask flyLayerMask;

    private bool canEat = true;

    void Update()
    {
        HandleInput();
    }

    private void HandleInput()
    {
        if (!GameManager.Instance.IsFinished && Input.GetMouseButtonDown(0) && canEat)
        {
            StartCoroutine(EatingCoroutine());
        }
    }

    private IEnumerator EatingCoroutine()
    {
        canEat = false;
        Fly fly = FindFly();
        if (fly != null && IsInRange(fly))
        {
            Eat(fly);
        }
        yield return new WaitForSeconds(eatingCooldown);
        canEat = true;
    }

    private Fly FindFly()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 100.0f, flyLayerMask.value))
        {
            return raycastHit.transform.GetComponentInParent<Fly>();
        }
        else
        {
            return null;
        }
    }

    private bool IsInRange(Fly fly)
    {
        return Vector3.Distance(transform.position, fly.transform.position) < eatingDistance;
    }

    private void Eat(Fly fly)
    {
        Vector3 lookAtPosition = fly.transform.position;
        lookAtPosition.y = transform.position.y;
        transform.LookAt(lookAtPosition);
        FlyEatenEvent flyEatenEvent = new FlyEatenEvent() { Fly = fly };
        flyEatenEvent.Invoke();
    }
}