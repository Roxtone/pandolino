﻿public class PlayerHiding : Singleton<PlayerHiding>
{
    private PlayerMovement playerMovement;
    private PlayerColor playerColor;

    public bool IsHidden { get; private set; }

    void Awake()
    {
        playerMovement = GetComponent<PlayerMovement>();
        playerColor = GetComponent<PlayerColor>();
        PlayerMovedEvent.AddListener(OnPlayerMoved);
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        PlayerMovedEvent.RemoveListener(OnPlayerMoved);
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    private void OnPlayerMoved(PlayerMovedEvent eventData)
    {
        IsHidden = eventData.Tile.ColorIndex == playerColor.ColorIndex;
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        IsHidden = playerMovement.CurrentTile.ColorIndex == eventData.Fly.ColorIndex;
    }
}