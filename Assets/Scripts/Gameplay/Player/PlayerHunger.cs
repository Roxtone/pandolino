﻿using UnityEngine;

public class PlayerHunger : Singleton<PlayerHunger>
{
    [SerializeField]
    private float maxHunger = 100.0f;
    [SerializeField]
    private float startHunger = 25.0f;
    [SerializeField]
    private float increaseRate = 5.0f;
    [SerializeField]
    private float flyValue = 25.0f;

    void Awake()
    {
        CurrentHunger = startHunger;
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void Update()
    {
        if (!GameManager.Instance.IsFinished)
        {
            IncreaseHunger();
            CheckForFinish();
        }
    }

    public float CurrentHunger { get; private set; }

    public float MaxHunger { get { return maxHunger; } }

    private void IncreaseHunger()
    {
        CurrentHunger += increaseRate * Time.deltaTime;
    }

    private void CheckForFinish()
    {
        if (CurrentHunger > maxHunger)
        {
            FinishGame();
        }
    }

    private void FinishGame()
    {
        GameFinishedEvent gameFinishedEvent = new GameFinishedEvent();
        gameFinishedEvent.Invoke();
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        CurrentHunger = Mathf.Max(CurrentHunger - flyValue, 0.0f);
    }
}