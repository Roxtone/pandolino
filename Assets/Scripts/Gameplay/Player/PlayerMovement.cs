﻿using DG.Tweening;
using System;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float movementDuration = 0.25f;
    [SerializeField]
    private float movementRange = 1.0f;
    [SerializeField]
    private float hopHeight = 0.25f;
    [SerializeField]
    private LayerMask tileLayerMask;

    private bool canMove = true;

    void Start()
    {
        FindStartingTile();
    }

    void Update()
    {
        HandleInput();
    }

    public Tile CurrentTile { get; private set; }

    private void FindStartingTile()
    {
        Ray ray = new Ray(transform.position + Vector3.up, Vector3.down);
        RaycastHit raycastHit;
        Physics.Raycast(ray, out raycastHit, 100.0f, tileLayerMask.value);
        CurrentTile = raycastHit.transform.GetComponentInParent<Tile>();
    }

    private void HandleInput()
    {
        if (!GameManager.Instance.IsFinished && Input.GetMouseButtonDown(0) && canMove)
        {
            Move();
        }
    }

    private void Move()
    {
        Tile tile = FindTile();
        if (tile != null && IsInRange(tile))
        {
            canMove = false;
            CurrentTile = tile;
            PlayerMovedEvent playerMovedEvent = new PlayerMovedEvent() { Tile = tile };
            playerMovedEvent.Invoke();
            AnimateMovement(tile.transform.position, () => canMove = true);
        }
    }

    private void AnimateMovement(Vector3 destination, Action onComplete)
    {
        transform.LookAt(destination);
        transform.DOJump(destination, hopHeight, 1, movementDuration).OnComplete(() => onComplete());
    }

    private Tile FindTile()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit raycastHit;
        if (Physics.Raycast(ray, out raycastHit, 100.0f, tileLayerMask.value))
        {
            return raycastHit.transform.GetComponentInParent<Tile>();
        }
        else
        {
            return null;
        }
    }

    private bool IsInRange(Tile tile)
    {
        float distance = Vector3.Distance(transform.position, tile.transform.position);
        return Mathf.Approximately(distance, movementRange);
    }
}