﻿using System.Collections;
using UnityEngine;

public class PlayerTongue : MonoBehaviour
{
    [SerializeField]
    private float animationDuration = 0.1f;

    private LineRenderer lineRenderer;

    void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        StartCoroutine(TongueCoroutine(eventData.Fly.transform.position));
    }

    private IEnumerator TongueCoroutine(Vector3 targetPosition)
    {
        float timer = 0.0f;
        while (timer < animationDuration)
        {
            Vector3 endPosition = Vector3.Lerp(targetPosition, transform.position, timer / animationDuration);
            lineRenderer.SetPositions(new Vector3[] { transform.position, endPosition });
            timer += Time.deltaTime;
            yield return null;
        }
        lineRenderer.SetPositions(new Vector3[] { Vector3.zero, Vector3.zero });
    }
}