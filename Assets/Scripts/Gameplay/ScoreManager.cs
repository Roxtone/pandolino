﻿using UnityEngine;

public class ScoreManager : Singleton<ScoreManager>
{
    [SerializeField]
    private int pointsPerSecond = 200;
    [SerializeField]
    private int pointsPerFly = 500;

    void Awake()
    {
        FlyEatenEvent.AddListener(OnFlyEaten);
    }

    void OnDestroy()
    {
        FlyEatenEvent.RemoveListener(OnFlyEaten);
    }

    void Update()
    {
        if (!GameManager.Instance.IsFinished)
        {
            UpdateScore();
        }
    }

    public int Score { get; private set; }

    private void UpdateScore()
    {
        Score += (int)(pointsPerSecond * Time.deltaTime);
    }

    private void OnFlyEaten(FlyEatenEvent eventData)
    {
        Score += pointsPerFly;
    }
}