﻿using UnityEngine;

public class Tile : ColorObject
{
    private int colorIndex;

    void Awake()
    {
        ColorIndex = Random.Range(0, ColorManager.Instance.ColorCount);
    }

    public int ColorIndex
    {
        get
        {
            return colorIndex;
        }
        set
        {
            colorIndex = value;
            ChangeColor(colorIndex);
        }
    }
}