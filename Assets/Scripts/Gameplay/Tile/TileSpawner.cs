﻿using UnityEngine;

public class TileSpawner : MonoBehaviour
{
    [SerializeField]
    private Tile tilePrefab;

    void Awake()
    {
        Spawn();
    }

    private void Spawn()
    {
        for (int y = 0; y < Board.Height; y++)
        {
            for (int x = 0; x < Board.Width; x++)
            {
                Spawn(x, y);
            }
        }
    }

    private void Spawn(int x, int y)
    {
        Vector3 position = Board.Instance.CalculateTileCenter(x, y);
        Instantiate(tilePrefab, position, Quaternion.identity, transform);
    }
}