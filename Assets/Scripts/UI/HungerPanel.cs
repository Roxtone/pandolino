﻿using UnityEngine;
using UnityEngine.UI;

public class HungerPanel : MonoBehaviour
{
    [SerializeField]
    private Image fill;

    void Update()
    {
        if (!GameManager.Instance.IsFinished)
        {
            UpdateFill();
        }
    }

    private void UpdateFill()
    {
        fill.fillAmount = 1.0f - PlayerHunger.Instance.CurrentHunger / PlayerHunger.Instance.MaxHunger;
    }
}