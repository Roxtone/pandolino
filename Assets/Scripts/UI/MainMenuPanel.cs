﻿using UnityEngine;
using UnityEngine.EventSystems;

public class MainMenuPanel : MonoBehaviour, IPointerClickHandler
{
    public void OnPointerClick(PointerEventData eventData)
    {
        LoadingController.Instance.LoadScene("Game");
    }
}