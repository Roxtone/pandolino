﻿using UnityEngine;
using UnityEngine.UI;

public class ScorePanel : MonoBehaviour
{
    [SerializeField]
    private Text valueText;

    void Update()
    {
        UpdateScore();
    }

    private void UpdateScore()
    {
        valueText.text = ScoreManager.Instance.Score.ToString();
    }
}