﻿using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SummaryPanel : MonoBehaviour, IPointerClickHandler
{
    [SerializeField]
    private Text scoreText;
    [SerializeField]
    private float minimumDuration = 2.0f;

    private bool reloadEnabled = false;

    void Awake()
    {
        GameFinishedEvent.AddListener(OnGameFinished);
        Hide();
    }

    void OnDestroy()
    {
        GameFinishedEvent.RemoveListener(OnGameFinished);
    }

    private void Hide()
    {
        gameObject.SetActive(false);
    }

    private void Show()
    {
        scoreText.text = string.Format(scoreText.text, ScoreManager.Instance.Score);
        gameObject.SetActive(true);
        StartCoroutine(EnableReloadCoroutine());
    }

    private IEnumerator EnableReloadCoroutine()
    {
        yield return new WaitForSeconds(minimumDuration);
        reloadEnabled = true;
    }

    private void OnGameFinished(GameFinishedEvent eventData)
    {
        Show();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (reloadEnabled)
        {
            LoadingController.Instance.LoadScene("MainMenu");
        }
    }
}